# Dotfiles

This is where I will store my dotfiles, mostly for backup purposes. Dotfiles are mostly configuration
files (in Linux and other Unix-based operating systems) which most software use to allow customisation.
In this repo, it includes dotfiles about Hyprland (WM for Wayland), DWM (which is a Window Manager for
Linux), Firefox (web browser), picom (compositor), Alacritty (terminal), and many more!

# Screenshot(s)

## Hyprland

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/0fAC8Aw38uA?si=Tj4MBrNiGWt29Rwy" frameborder="0" allowfullscreen="true"></iframe>
</figure>

![Hyprland Desktop](assets/hyprland-desktop.png)

## Dwm

![Desktop](assets/desktop.png)
