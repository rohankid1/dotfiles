if doas make clean install 1> /dev/null; then
  # In my case, 1 and 2 are respectively red and green;
  # it may be different for you, so you may need to check
  # what your terminal colours are.
  tput setaf 2; echo "Pass"
else
  tput setaf 1; echo "Fail"
fi