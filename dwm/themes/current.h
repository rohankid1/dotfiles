static const unsigned int borderpx = 1;
static const unsigned int snap = 32;
static const unsigned int gappx = 30;
static const Gap default_gap = {.isgap = 1, .realgap = 30, .gappx = 30 };

// Bar
// static const int vertpad = 0;
// static const int sidepad = 0;
static const int showbar = 1; // 1 = show
static const int topbar = 1;  // 0 = bottom
// static const int usealtbar = 0; // 1 = use non-dwm status bar
// static const char *altbarclass = "Polybar"; // alternate bar class name
// static const char *altbarcmd = ""; // alternate bar launch command

// Alpha 
static const unsigned int baralpha = 0xd0;
static const unsigned int borderalpha = OPAQUE;

// Fonts
static const char *fonts[] = {"Noto Sans Mono Medium:size=10"};
static const char dmenufont[] = "Noto Sans Mono Medium:size=10";

// Colours
// static const char gray1[] = "#4A148C";
static const char gray1[] = "#1e1e2e";
static const char gray2[] = "#444444";
static const char gray3[] = "#EDE7F6";
static const char gray4[] = "#5C6BC0";
static const char cyan[] = "#000000";
