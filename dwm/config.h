#include <stddef.h>

// Constants
#define TERMINAL "alacritty"

// Theme
#include "themes/current.h"

// Tags
static const char *tags[] = {"Web", "Social", "Dev", "Game", "Study", "Other"};


static const char *colors[][3] = {
    /*                     fg        bg        border  */
    [SchemeNorm]    =    { gray3,    gray1,    gray2 },
    [SchemeSel]    =     { gray4,    cyan,    cyan },
};

// Application shortcuts
static const char *launcher[] = { "/home/rohan/.config/rofi/bin/runner", NULL };
static const char *flameshot[] = { "flameshot", "gui", NULL };
static const char *prtscr[] = { "flameshot", "full", "-c" };
 
// Layouts
static const float mfact = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster = 1;    /* number of clients in master area */
static const int resizehints = 0; /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */


static const Rule rules[] = {
    /* class    instance    title    tags_mask    isfloating    monitor   */
    { "Gimp",    NULL,    NULL,    0,    1,    -1 },
    { "Firefox",    NULL,    NULL,    1 << 8,    0,    -1 },
};


// Fibonacci Layouts
#include "fibonacci.c"

//   symbol, func
static const Layout layouts[] = {
    {"[]=", tile},
    {"[\\]", dwindle},
    {"[@]", spiral}, /* first entry is default */
    {"><>", NULL}, /* no layout function means floating behavior */
    {"[M]", monocle},
};

// Commands
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", gray1, "-nf", gray3, "-sb", cyan, "-sf", gray4, NULL};
static const char *termcmd[] = {TERMINAL, NULL};

// Key definitions
#define MODKEY Mod4Mask
#define TAGKEYS(KEY, TAG)                                                      \
  {MODKEY, KEY, view, {.ui = 1 << TAG}},                                       \
      {MODKEY | ControlMask, KEY, toggleview, {.ui = 1 << TAG}},               \
      {MODKEY | ShiftMask, KEY, tag, {.ui = 1 << TAG}},                        \
      {MODKEY | ControlMask | ShiftMask, KEY, toggletag, {.ui = 1 << TAG}},

// modifier,  key,  function,  argument
#include "shiftview.c"
static const Key keys[] = {
    {MODKEY | ShiftMask,    XK_Return,    spawn,    {.v = termcmd}},
    {MODKEY,    XK_s,    spawn,    {.v = flameshot}},
    {MODKEY,    XK_p,    spawn,    {.v = launcher}},
    {0,     XK_Print, spawn,    {.v = prtscr}},
    {MODKEY, XK_n, shiftview, {.i = +1}},
    {MODKEY, XK_b, shiftview, {.i = -1}},
    {MODKEY, XK_j, focusstack, {.i = +1}},
    {MODKEY, XK_k, focusstack, {.i = -1}},
    {MODKEY, XK_h, setmfact, {.f = -0.05}},
    {MODKEY, XK_l, setmfact, {.f = +0.05}},
    {MODKEY, XK_Return, zoom, {0}},
    {MODKEY, XK_Tab, view, {0}},
    {MODKEY | ShiftMask, XK_c, killclient, {0}},
    {MODKEY, XK_t, setlayout, {.v = &layouts[0]}},
    {MODKEY, XK_f, setlayout, {.v = &layouts[1]}},
    {MODKEY, XK_m, setlayout, {.v = &layouts[2]}},
    {MODKEY, XK_r, setlayout, {.v = &layouts[3]}},
    {MODKEY | ShiftMask,    XK_r,    setlayout,    { .v = &layouts[4] }},
    {MODKEY, XK_space, setlayout, {0}},
    {MODKEY | ShiftMask, XK_space, togglefloating, {0}},
    { MODKEY | ShiftMask, XK_f, togglefullscr, {0} },
    {MODKEY, XK_0, view, {.ui = ~0}},
    {MODKEY | ShiftMask, XK_0, tag, {.ui = ~0}},
    {MODKEY, XK_comma, focusmon, {.i = -1}},
    {MODKEY, XK_period, focusmon, {.i = +1}},
    {MODKEY | ShiftMask, XK_comma, tagmon, {.i = -1}}, {MODKEY | ShiftMask, XK_period, tagmon, {.i = +1}},
    // circular movement up down the stack
    // move window down the stack
    {MODKEY | ShiftMask, XK_j, movestack, {.i = +1}},
    // move window up the stack
    {MODKEY | ShiftMask, XK_k, movestack, {.i = -1}},
    // end of circular movement code
    TAGKEYS(XK_1, 0) TAGKEYS(XK_2, 1) TAGKEYS(XK_3, 2) TAGKEYS(XK_4, 3)
        TAGKEYS(XK_5, 4) TAGKEYS(XK_6, 5) TAGKEYS(XK_7, 6) TAGKEYS(XK_8, 7)
            TAGKEYS(XK_9, 8){MODKEY | ShiftMask, XK_q, quit, {0}},
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle,
 * ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
    /* click                event mask      button          function argument
     */
    {ClkLtSymbol, 0, Button1, setlayout, {0}},
    {ClkLtSymbol, 0, Button3, setlayout, {.v = &layouts[2]}},
    {ClkWinTitle, 0, Button2, zoom, {0}},
    {ClkStatusText, 0, Button2, spawn, {.v = termcmd}},
    {ClkClientWin, MODKEY, Button1, movemouse, {0}},
    {ClkClientWin, MODKEY, Button2, togglefloating, {0}},
    {ClkClientWin, MODKEY, Button3, resizemouse, {0}},
    {ClkTagBar, 0, Button1, view, {0}},
    {ClkTagBar, 0, Button3, toggleview, {0}},
    {ClkTagBar, MODKEY, Button1, tag, {0}},
    {ClkTagBar, MODKEY, Button3, toggletag, {0}},
};