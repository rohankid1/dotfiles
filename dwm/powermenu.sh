#!/bin/bash

function powermenu {
    options="Cancel\nShutdown\nRestart\nSleep"
    selected=$(echo -e $options | dmenu)
    if [[ $selected = "Shutdown" ]]; then
        systemctl poweroff
    elif [[ $selected = "Restart" ]]; then
        /sbin/reboot
    elif [[ $selected = "Sleep" ]]; then
        systemctl suspend
    elif [[ $selected = "Cancel" ]]; then
        echo "Canceled"
    fi
}

powermenu