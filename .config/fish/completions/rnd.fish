# Put this file in ~/.config/fish/completions/

complete -c rnd -s h -l help -d "Print help information"

complete -c rnd -s m -l min -d "Set the minimum value"
complete -c rnd -s M -l max -d "Set the maximum value"
