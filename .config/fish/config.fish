if not string match -q /home/rohan/.cargo $PATH
    set -x PATH /home/rohan/.cargo $PATH
end

if status is-interactive
    # Commands to run in interactive sessions can go here
end

# ALIASES
alias ls "eza -la --color=always --group-directories-first"
alias bat "batcat --theme='Catppuccin-mocha'"
alias .. 'z ..'
alias ... 'z ../..'
alias fd fdfind
# alias emacs "emacsclient -c -a 'emacs'"

alias apts "apt search"
alias aptin "sudo apt install"
alias aptup "sudo -A apt update && doas apt upgrade"
alias aptupd "sudo apt update"
alias aptupg "sudo -A apt upgrade"
alias aptrm "sudo -A apt remove"
alias aptautorm "sudo apt autoremove"


alias s "nala search"
alias up "sudo -A nala upgrade"
alias nalarm "sudo -A nala remove"
alias nalain "sudo nala install"
alias nalaautorm "sudo nala autoremove"

cat ~/.cache/wal/sequences
macchina
zoxide init fish | source
starship init fish | source
