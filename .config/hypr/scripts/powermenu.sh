#!/bin/bash

# Options
shutdown="Power Off"
reboot="Restart"
lock="Lock"
suspend="Suspend"
logout="Log out"

chosen=$(echo -e "$shutdown\n$reboot\n$suspend\n$logout" | rofi -dmenu -p "Power Menu" -theme ~/.config/rofi/powermenu.rasi)
case $chosen in
  $shutdown)
        systemctl poweroff
    ;;
  $reboot)
        systemctl reboot
    ;;
  $suspend)
        systemctl suspend
    ;;
  $logout)
        hyprctl dispath exit 0
    ;;
esac
