#!/bin/bash
DIR=$HOME/Pictures/wallpapers
PICS=($(ls ${DIR}))

RANDOMPIC=${PICS[ $RANDOM % ${#PICS[@]} ]}

if [[  $(pidof hyprpaper) ]]; then
  pkill hyprpaper
fi

if ! [[ $(pidof swww-daemon) ]]; then
  swww-daemon & disown
fi

swww query || swww init

swww img ${DIR}/${RANDOMPIC} --transition-fps 60 --transition-type any --transition-duration 2
