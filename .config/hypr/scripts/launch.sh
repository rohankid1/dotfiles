#!/bin/bash
export SDL_VIDEODRIVER=wayland
export _JAVA_AWT_WM_NONREPARENTING=1
export QT_QPA_PLATFORM=wayland
export XDG_CURRENT_DESKTOP=Hyprland
export XDG_SESSION_DESKTOP=Hyprland
export XDG_SESSION_TYPE=wayland


swww-daemon &
waybar &
hyprpaper &
firefox &
discord --enable-features=UseOzonePlatform --ozone-platform=wayland &
dunst
