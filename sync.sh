#!/bin/bash

path="/home/$USER/Desktop/dotfiles/.config" # destination of where you want stuff to be copied to
config="/home/$USER/.config"
stuff=(
  "$config/alacritty/"
  "$config/starship.toml"
  "$config/picom"
  "$config/rofi"
  "$config/sxhkd"
  "$config/obs-studio"
  "$config/nitrogen"
  "$config/bat"
  "$config/fish"
  "$config/dunst"
  "$config/macchina"
  "$config/hypr"
  "$config/waybar"
)

addDwm() {
  dwm="/home/$USER/Programs/dwm/"
  location="/home/$USER/Desktop/dotfiles/"

  cp -r $dwm $location
  rm -rf $location/dwm/.git/ $location/dwm/.vscode/
}

addDwmBlocks() {
  blocks="/home/$USER/Programs/dwmblocks/"
  location="/home/$USER/Desktop/dotfiles/"

  cp -r $blocks $location
  rm -rf $location/dwmblocks/.git/ $location/dwmblocks/.gitignore
}

addFF() {
  profile="abyr3j3f.default-release"
  ff="/home/rohan/.mozilla/firefox/$profile/chrome/"
  location="$path/../.mozilla/firefox/$profile/chrome/"

  cp -rf $ff $location
}

for str in ${stuff[@]}; do
  cp -rf $str $path
done

addDwm
addDwmBlocks
addFF
